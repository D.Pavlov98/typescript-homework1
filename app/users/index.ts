import { usersArray } from "./users";
import { usersInfoArray } from "./usersInfo";
import{Iusers} from "../models/Iusers";


interface IUserNew{
    name:string,
    position:string,
    age:number,
    gender:string
}

function getUsersJobPositions(usersArray:Iusers[]):IUserNew[]{
    const newUsersArray:IUserNew[]=[];
    if(Array.isArray(usersArray)){
        usersArray.forEach((usersItem)=>{
            const userInfo=usersInfoArray.find(item=>item.userid===usersItem.userid)
            if(userInfo){
                const newUsersArr:IUserNew={
                name: usersItem.name,
                position: userInfo.organization.position,
                age: userInfo.age,
                gender: usersItem.gender
                }
                newUsersArray.push(newUsersArr);
            }
        })
    }
    return newUsersArray;
}
const usersPositions = getUsersJobPositions(usersArray);
console.log('userPositions', usersPositions);
